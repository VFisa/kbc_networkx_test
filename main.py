"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2016, Twitter: @VFisa'"
"https://www.python-course.eu/networkx.php"


import os
import sys
import json
import networkx as nx
from networkx.readwrite import json_graph
import pprint
import matplotlib.pyplot as plt


## Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

COLOURS = {"transformation": "r", "storage" : "b"}

def read_json_file(filename):
    with open(filename) as f:
        js_graph = json.loads(f.read())
        js_graph["links"] = js_graph.pop("transitions")
        for a in js_graph["nodes"]:
            a["id"] = a["label"]
        ## DIRTY STUFF
        for a in js_graph["nodes"]:
            a["color"] = COLOURS[a["object"]["type"]]
        #pprint.pprint(js_graph)
    return json_graph.node_link_graph(js_graph, multigraph=False, directed=True)

filename = "example.json"

final_graph = read_json_file(filename)

# https://python-graph-gallery.com/324-map-a-color-to-network-nodes/
# https://python-decompiler.com/article/2013-11/how-to-draw-directed-graphs-using-networkx-in-python

print(range(final_graph))

nx.draw(final_graph, node_color="blue", linewidths=0.5, font_size=6, font_weight="normal", with_labels=True, weight="distance", pos=nx.spring_layout(final_graph))

plt.show()
#plt.savefig("path.png")
